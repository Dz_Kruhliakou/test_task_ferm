package by.dkruhliakou.ferm.Counteiner;

public interface IContainer<E> {
	int size();
	boolean add(E element);
	boolean remove(Object obj);
}
