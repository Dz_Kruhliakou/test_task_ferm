package by.dkruhliakou.ferm.Counteiner;

import java.util.NoSuchElementException;

import org.omg.PortableServer.THREAD_POLICY_ID;

public class Counteiner<E> implements IContainer<E> {
	private Node<E> header = new Node<E>(null, null, null);
	private int size = 0;
	
	public Counteiner() {
		header.next = header.previous = header;
	}
	
	public int size() {
		return size;
	}

	public boolean add(E element) {
		Node<E> newNode = new Node<E>(element, header, header.previous);
		newNode.previous.next = newNode;
		newNode.next.previous = newNode;
		incSize();
		return true;
	}

	public boolean remove(Object obj) {
		if (obj == null) {
			for (Node<E> it = header.next; it != header; it = it.next)
				if (it.element == null) {
					remove(it);
					return true;
				}
		} else {
			for (Node<E> it = header.next; it != header; it = it.next)
				if (obj.equals(it.element)) {
					remove(it);
					return true;
				}
		}
		return false;
	}
	
	private void remove(Node<E> node) {
		if (node == header)
			throw new NoSuchElementException();
		
		 node.previous.next = node.next;
		 node.next.previous = node.previous;
		 node.next = node.previous = null;
		 node.element = null;
		 decSize();
		 
	}
	
	private void decSize() {
		size--;
	}
	
	private void incSize() {
		size++;
	}
	
	@Override
	public String toString() {
		String result = "";
		for (Node<E> it = header.next; it != header; it = it.next)
			result += it.element.toString() + "\n";
		return result;
	}

	private static class Node<E> {
		E element;
		Node<E> next;
		Node<E> previous;
		
		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		
	}

	

	

}
