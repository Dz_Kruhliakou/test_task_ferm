package by.dkruhliakou.ferm;

import java.util.LinkedList;
import java.util.List;

import by.dkruhliakou.ferm.beans.Ferm;
import by.dkruhliakou.ferm.beans.FermCustom;


public class Runner {

	public static void main(String[] args) {
		
		Ferm milkFerm = new Ferm();
		milkFerm.giveBirth(1, 2, "Cow1");
		milkFerm.giveBirth(1, 3, "Cow2");
		milkFerm.giveBirth(1, 4, "Cow3");
		milkFerm.printFerm();
		milkFerm.endLifeSpan(3);
		System.out.println("");
		milkFerm.printFerm();
		System.out.println("Custom");
		
		FermCustom milkFetmCustom = new FermCustom();
		milkFetmCustom.giveBirth(1, 2, "Cow1");
		milkFetmCustom.giveBirth(1, 3, "Cow2");
		System.out.println(milkFetmCustom);
		milkFetmCustom.endLifeSpan(3);
		System.out.println(milkFetmCustom);

	}

}
