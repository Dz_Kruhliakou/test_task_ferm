package by.dkruhliakou.ferm.beans;

import java.util.LinkedList;
import java.util.List;

public class Ferm {
	List<Cow> cows = new LinkedList<Cow>();

	public Ferm() {
		cows.add(new Cow(1, "Cow0"));
	}

	public void giveBirth(int parentCowId, int childCowId, String childNickName) {
		cows.add(new Calf(parentCowId, childCowId, childNickName));
	}
	
	public void endLifeSpan (int cowId) {
		Cow killCow = new Cow(cowId, "");
	
			cows.remove(killCow);
	}
	
	public void printFerm() {
		for (Cow cow: cows)
			System.out.println(cow);
	}
}
