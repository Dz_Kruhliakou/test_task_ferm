package by.dkruhliakou.ferm.beans;

public class Cow {
	private int cowId;
	private String nickName;
	
	public Cow() {
		super();
	}

	public Cow(int cowId, String nickName) {
		this.cowId = cowId;
		this.nickName = nickName;
	}
	
	public int getCowId() {
		return cowId;
	}

	public void setCowId(int cowId) {
		this.cowId = cowId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Cow calfBirth() {
		return new Calf();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cowId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Cow))
			return false;
		Cow other = (Cow) obj;
		if (cowId != other.cowId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "cowId: " + cowId + "; " + "nickName: " + nickName;
	}
	
	
	
}
