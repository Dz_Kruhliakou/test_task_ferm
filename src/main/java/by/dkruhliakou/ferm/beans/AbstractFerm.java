package by.dkruhliakou.ferm.beans;

public abstract class AbstractFerm {
	
	public abstract void giveBirth(int parentCowId, int childCowId, String childNickName);
	public abstract void endLifeSpan (int cowId);
}
