package by.dkruhliakou.ferm.beans;

import by.dkruhliakou.ferm.Counteiner.Counteiner;

public class FermCustom extends AbstractFerm {
	Counteiner<Cow> cows = new Counteiner<Cow>();
	
	public FermCustom() {
		cows.add(new Cow(1, "Cow0"));
	}
	
	@Override
	public void giveBirth(int parentCowId, int childCowId, String childNickName) {
		cows.add(new Calf(parentCowId, childCowId, childNickName));
		
	}

	@Override
	public void endLifeSpan(int cowId) {
		Cow killCow = new Cow(cowId, "");
		cows.remove(killCow);
	}

	@Override
	public String toString() {
		return cows.toString();
	}

	
}
