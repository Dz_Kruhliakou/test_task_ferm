package by.dkruhliakou.ferm.beans;

public class Calf extends Cow {
	private int parentCowId;
	
	public Calf() {
		super();
	}
	
	public Calf(int parentCowId, int cowId, String nickName) {
		super(cowId, nickName);
		this.parentCowId = parentCowId;
	}

	public int getParentCowId() {
		return parentCowId;
	}
	
}
